//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                
#include <fstream>
#include <sstream>

#include <boost/algorithm/string/trim.hpp>
#include <boost/functional/hash.hpp>

#include "GGAExceptions.hpp"
#include "GGAInstance.hpp"
#include "GGAOptions.hpp"
#include "OutputLog.hpp"

//==============================================================================
// GGAInstance public static methods

/**
 * Creates a GGAInstance from a line in an instance-seed file taking the 
 * following format:
 *                     <seed> <instance> <extra1 ... extra n>
 *
 * extra could be anything, perhaps optimal values for those instances, correct
 * program output, etc
 */
GGAInstance GGAInstance::fromString(const std::string& inst_str)
{
    const GGAOptions& opts = GGAOptions::instance();
    int seed;
    std::string instance;
    StringVector extra;

    std::stringstream ss(inst_str);

    if (!(ss >> seed)) { // NC_DETERMINISTIC
        ss.str("");
        ss.clear();
        ss << inst_str; 
        seed = 1;
    }

    if (!(ss >> instance))
        throw GGAMalformedInstanceException(inst_str);
    boost::algorithm::trim(instance);

    std::string tmp;
    while (ss >> tmp) {
        boost::algorithm::trim(tmp);
        extra.push_back(tmp);
    }
    
    return GGAInstance(seed, opts.target_algo_cpu_limit, instance, extra);
}

//==============================================================================
// GGAInstance public methods

GGAInstance::GGAInstance() 
    : m_seed(-1)
    , instance_()
    , extra_()
{ }

GGAInstance::GGAInstance(const GGAInstance& other)
    : m_seed(other.m_seed)
    , instance_(other.instance_)
    , extra_(other.extra_)
{ }

GGAInstance::GGAInstance(int seed, double timeout, const std::string& instance,
                         const StringVector& extra) 
    : m_seed(seed)
    , instance_(instance)
    , extra_(extra) 
{ }

GGAInstance::~GGAInstance() 
{ }

GGAInstance& GGAInstance::operator=(const GGAInstance& o)
{
    m_seed = o.m_seed;
    instance_ = o.instance_;
    extra_ = o.extra_;

    return *this;
}

std::string GGAInstance::toString() const
{
    std::stringstream ss;
    ss << "[GGAInstance: " << this << "; Seed: " << m_seed 
       << "; Instance: " << instance_ << "; Extra: " <<
       OutputLog::stringVectorToString(extra_) << "]";
    return ss.str();
}


// =====================================
// === GGAInstance::STLMapComparator ===
// =====================================

/**
 *
 */
bool GGAInstance::STLMapComparator::operator()(const GGAInstance& inst1,
                                               const GGAInstance& inst2) const
{
    static boost::hash<std::string> string_hash;
    static boost::hash<StringVector> strvec_hash;
    static boost::hash<int> int_hash;

    size_t seed1 = 0;
    boost::hash_combine(seed1, string_hash(inst1.getInstance()));
    boost::hash_combine(seed1, int_hash(inst1.getSeed()));
    boost::hash_combine(seed1, strvec_hash(inst1.getExtra()));

    size_t seed2 = 0;
    boost::hash_combine(seed2, string_hash(inst2.getInstance()));
    boost::hash_combine(seed2, int_hash(inst2.getSeed()));
    boost::hash_combine(seed2, strvec_hash(inst2.getExtra()));

    return seed1 < seed2;
}


// =========================== //
// ===== GGAInstanceInfo ===== //
// =========================== //

GGAInstanceInfo::GGAInstanceInfo()
    : estimatedExecutionTime_(-1)
{ }

GGAInstanceInfo::GGAInstanceInfo(const GGAInstanceInfo& orig)
    : estimatedExecutionTime_(orig.estimatedExecutionTime_)
{ }

GGAInstanceInfo::~GGAInstanceInfo()
{ }

GGAInstanceInfo& GGAInstanceInfo::operator=(const GGAInstanceInfo& orig)
{
    estimatedExecutionTime_ = orig.estimatedExecutionTime_;
    return *this;
}
