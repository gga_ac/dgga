//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                

#include <boost/scoped_ptr.hpp>
#include <boost/filesystem.hpp>


#include "gga.hpp"
#include "ggatypedefs.hpp"
#include "GGAExceptions.hpp"
#include "GGAGenome.hpp"
#include "GGAInstance.hpp"
#include "GGALocalSelector.hpp"
#include "GGAOptions.hpp"
#include "GGAPopulation.hpp"
#include "GGATournament.hpp"
#include "GGAUtil.hpp"
#include "OutputLog.hpp"

// Function prototypes
//

/**
 *
 */
int runGGA() 
{
    const GGAOptions& opts = GGAOptions::instance();
    
    GGAInstances instances;
    parseLocalData(&instances);

    // Initialize tournament + add population
    GGALocalSelector selector;
        
    GGATournament tournament(instances, selector);
    tournament.populate();  

    if(opts.nc_execdir != "")
        boost::filesystem::current_path(opts.nc_execdir.c_str());
    
    // Run tournament
    tournament.run();
    
    // No processes should be running at this point, but just in case...
    //kill(0, SIGTERM);
    return 0;
}

/**
 *
 */
void parseLocalData(GGAInstances* instances)
{
    GGAOptions& opts = GGAOptions::mutableInstance();

    OutputLog::setReportLevel(static_cast<OutputLog::Level>(opts.verbosity));
    printOptions();

    if (!boost::filesystem::exists(opts.param_tree_file))
        throw GGAFileNotFoundException(opts.param_tree_file);

    // Parse parameter Tree
    GGAParameterTree& paramTree = GGAParameterTree::mutableInstance();  
    paramTree.parseTreeFile(opts.param_tree_file);
    LOG("Parameter Tree:" << std::endl << paramTree.toString());
    LOG("Command: " << std::endl << paramTree.command()->rawCommand());
    
    // Parse instances
    instances->loadInstancesFile(opts.instance_seed_file);
    if (!opts.clusters_file.empty()) {
        if (!boost::filesystem::exists(opts.clusters_file))
            throw GGAFileNotFoundException(opts.clusters_file);
        instances->loadInstanceClustersFile(opts.clusters_file);
    }
}
