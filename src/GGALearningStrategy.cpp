//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                

#include <algorithm>
#include "GGALearningStrategy.hpp"

//==============================================================================
// GGALearningStrategy public methods

/**
 * Initializes a learning strategy with some instances.
 */
GGALearningStrategy::GGALearningStrategy(const GGAInstances& instances)
    : m_instances(instances)
{ }


/**
 *
 */
GGALearningStrategy::GGALearningStrategy(const GGALearningStrategy& other)
    : m_instances(other.m_instances)
{ }


/**
 *
 */
GGALearningStrategy::~GGALearningStrategy()
{ }

/**
 * @brief Selects n instances randomly.
 */
GGAInstanceVector GGALearningStrategy::selectRandomInstances(size_t n) const
{
    GGAInstanceVector retInsts(m_instances.getAllInstances());
    std::random_shuffle(retInsts.begin(), retInsts.end());
    retInsts.resize(n > retInsts.size() ? retInsts.size() : n);
    
    return retInsts;
}

GGAInstanceVector GGALearningStrategy::selectRandomClusterInstances(
        unsigned index, unsigned n) const
{
    std::vector<unsigned> cluster = m_instances.getCluster(index);
    std::random_shuffle(cluster.begin(), cluster.end());
    cluster.resize(n > cluster.size() ? cluster.size() : n);

    GGAInstanceVector retInsts;   
    for (unsigned i = 0; i < cluster.size(); ++i)
        retInsts.push_back(m_instances.getInstance(cluster[i]));
    
    return retInsts;
}
