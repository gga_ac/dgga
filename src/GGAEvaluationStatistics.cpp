//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                
#include "GGAEvaluationStatistics.hpp"

// --------------------- //
// GGAInstanceStatistics //
// --------------------- //

GGAEvaluationStatistics::GGAEvaluationStatistics()
    : m_avg_performance(0.0)
    , m_nevaluations(0)
{ }

GGAEvaluationStatistics::GGAEvaluationStatistics(const GGAEvaluationStatistics& o)
    : m_avg_performance(o.m_avg_performance)
    , m_nevaluations(o.m_nevaluations)
{ }

GGAEvaluationStatistics::~GGAEvaluationStatistics()
{ }

GGAEvaluationStatistics& GGAEvaluationStatistics::operator =(
        const GGAEvaluationStatistics& o)
{
    m_avg_performance = o.m_avg_performance;
    m_nevaluations = o.m_nevaluations;
    return *this;
}

void GGAEvaluationStatistics::addEvaluation(double performance)
{
    if (m_nevaluations > 0)
        m_avg_performance = (m_avg_performance + performance) / 2;
    else
        m_avg_performance = performance;
    m_nevaluations += 1;
}

void GGAEvaluationStatistics::addEvaluation(
        const GGAEvaluationStatistics& evals)
{
    if (m_nevaluations > 0)
        m_avg_performance = (m_avg_performance + evals.m_avg_performance) / 2;
    else
        m_avg_performance = evals.m_avg_performance;
    m_nevaluations += evals.m_nevaluations;
}
