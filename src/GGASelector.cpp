//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                


#include "GGASelector.hpp"


// ----------------- //
// GGASelectorResult //
// ----------------- //

GGASelectorResult::GGASelectorResult()
    : num_evaluations_(0)
    , winners_()
    , instancesPerformance_()
{ }

GGASelectorResult::GGASelectorResult(const GGASelectorResult& o)
    : num_evaluations_(o.num_evaluations_)
    , winners_(o.winners_)
    , instancesPerformance_(o.instancesPerformance_)
{ }

GGASelectorResult::~GGASelectorResult()
{ }
    
GGASelectorResult& GGASelectorResult::operator=(const GGASelectorResult& o)
{
    this->num_evaluations_ = o.num_evaluations_;
    this->winners_ = o.winners_;
    this->instancesPerformance_ = o.instancesPerformance_;
    
    return *this;
}

int GGASelectorResult::getNumEvaluations() const             { return num_evaluations_; }
const GGAGenomeVector& GGASelectorResult::getWinners() const { return winners_; }

bool GGASelectorResult::hasInstancePerformance(const GGAInstance& instance) const 
{ return instancesPerformance_.count(instance) > 0; }

const std::vector<double>& 
GGASelectorResult::getInstancePerformance(const GGAInstance& instance) const 
{ return mapAt(instancesPerformance_, instance); }


// private methods
// -------------------------------------------------------------------------------------------------

GGASelectorResult::GGASelectorResult(int num_evaluations,
                                     const GGAGenomeVector& winners,
                                     const InstancePerformancesMap& performance)
    : num_evaluations_(num_evaluations)
    , winners_(winners)
    , instancesPerformance_(performance)
{ }


// ------------------------ //
// GGASelectorResultBuilder //
// ------------------------ //

GGASelectorResultBuilder::GGASelectorResultBuilder()
    : num_evaluations_(0)
    , winners_()
    , instancesPerformance_()
{ }

GGASelectorResultBuilder::~GGASelectorResultBuilder()
{ }

GGASelectorResultBuilder& GGASelectorResultBuilder::addAll(const GGASelectorResult& result)
{
    num_evaluations_ += result.getNumEvaluations();
    winners_.insert(winners_.end(), result.getWinners().begin(), result.getWinners().end());
    
    const GGASelectorResult::InstancePerformancesMap& performanceMap = result.instancesPerformance_;
    GGASelectorResult::InstancePerformancesMap::const_iterator it, end = performanceMap.end();
    for (it = performanceMap.begin(); it != end; ++it)
        addInstancePerformance(it->first, it->second);
    return *this;
}

GGASelectorResultBuilder& GGASelectorResultBuilder::addWinner(const GGAGenome& winner)
{
    winners_.push_back(winner);
    return *this;
}

GGASelectorResultBuilder& GGASelectorResultBuilder::addWinners(const GGAGenomeVector& winners)
{
    winners_.insert(winners_.end(), winners.begin(), winners.end());
    return *this;
}

GGASelectorResultBuilder& GGASelectorResultBuilder::addInstancePerformance(
        const GGAInstance& instance, double performance)
{
    if (instancesPerformance_.count(instance) == 0) {
        instancesPerformance_[instance] = GGASelectorResult::InstancePerformancesMap::mapped_type();
    }
    
    instancesPerformance_[instance].push_back(performance);
            
    return *this;
}

GGASelectorResultBuilder& GGASelectorResultBuilder::addInstancePerformance(
        const GGAInstance& instance, const std::vector<double>& performance)
{
    if (instancesPerformance_.count(instance) == 0) {
        instancesPerformance_[instance] = GGASelectorResult::InstancePerformancesMap::mapped_type();
    }
    
    GGASelectorResult::InstancePerformancesMap::mapped_type& perf = instancesPerformance_[instance];
    perf.insert(perf.end(), performance.begin(), performance.end());
            
    return *this;
}

GGASelectorResultBuilder& GGASelectorResultBuilder::increaseNumEvaluations(int num_evaluations)
{
    num_evaluations_ += num_evaluations;
    return *this;
}

GGASelectorResult GGASelectorResultBuilder::build() const
{
    return GGASelectorResult(num_evaluations_, winners_, instancesPerformance_);
}

void GGASelectorResultBuilder::clear()
{
    num_evaluations_ = 0;
    winners_.clear();
    instancesPerformance_.clear();
}
