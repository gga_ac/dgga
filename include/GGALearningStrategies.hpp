//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                
#ifndef _GGA_LEARNING_STRATEGIES_HPP_
#define _GGA_LEARNING_STRATEGIES_HPP_

#include "GGALearningStrategy.hpp"


/** === GGALearningStrategyLinear === **/

class GGALearningStrategyLinear : public GGALearningStrategy 
{
public:
    GGALearningStrategyLinear(const GGAInstances& instances);
    virtual ~GGALearningStrategyLinear();
    virtual GGAInstanceVector instances(int generation) const;
};


/** === GGALearningStrategyStep === **/

class GGALearningStrategyStep : public GGALearningStrategy 
{
public:
    GGALearningStrategyStep(const GGAInstances& instances);
    virtual ~GGALearningStrategyStep();
    virtual GGAInstanceVector instances(int generation) const;
};


/** === GGALearningStrategyParabola === **/

class GGALearningStrategyParabola : public GGALearningStrategy 
{
public:
    GGALearningStrategyParabola(const GGAInstances& instances);
    virtual ~GGALearningStrategyParabola();
    virtual GGAInstanceVector instances(int generation) const;

private:
    double m_a;
};


/** === GGALearningStrategyExp === **/

class GGALearningStrategyExp : public GGALearningStrategy 
{
public:
    GGALearningStrategyExp(const GGAInstances& instances);
    virtual ~GGALearningStrategyExp();
    virtual GGAInstanceVector instances(int generation) const;

private:
    double m_a;
};

/** === GGALearningStrategyClusterUniformSelection === **/

class GGALearningStrategyRndCluster : public GGALearningStrategy
{
public:
    GGALearningStrategyRndCluster(const GGAInstances& instances);
    virtual ~GGALearningStrategyRndCluster();
    virtual GGAInstanceVector instances(int generation) const;
    
private:
    
};

#endif
