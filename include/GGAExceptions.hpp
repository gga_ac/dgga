//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                

#ifndef _GGA_EXCEPTIONS_HPP_
#define _GGA_EXCEPTIONS_HPP_

#include <exception>
#include <string>

/**
 * @brief Base GGAException
 */
class GGAException : public std::exception 
{
public:
    GGAException();
    GGAException(const GGAException&);
    GGAException(const std::string&);
    virtual ~GGAException() throw();

    virtual const char* what() const throw();

    const std::string& message() const;
protected:
    void message(const std::string&);
private:
    std::string m_message;
};

/**
 *
 */
class GGAMalformedInstanceException : public GGAException
{
public:
    GGAMalformedInstanceException(const std::string& inst);
    GGAMalformedInstanceException(const std::string& inst, const std::string& file,
                               int line);
};

/**
 *
 */
class GGAFileNotFoundException : public GGAException
{
public:
    GGAFileNotFoundException(const std::string& file);
};

/**
 *
 */
class GGAOptionsException : public GGAException
{
public:
    GGAOptionsException(const std::string& prob);
};

/**
 *
 */
class GGAParameterException : public GGAException
{
public:
    GGAParameterException(const std::string& prob);
};

/**
 *
 */
class GGAPopulationException : public GGAException
{
public:
    GGAPopulationException(const std::string& prob);
};

/**
 *
 */
class GGAXMLParseException : public GGAException
{
public:
    GGAXMLParseException(const std::string& msg);
};

/**
 *
 */
class GGACommandException : public GGAException
{
public:
    GGACommandException(const std::string& msg);
};

/**
 *
 */
class GGAScenarioFileException : public GGAException
{
public:
    GGAScenarioFileException(const std::string& msg);
};

/**
 *
 */
class GGAInterruptedException : public GGAException
{
public:
    GGAInterruptedException(const std::string& msg);
};

/**
 *
 */
class GGANullPointerException : public GGAException
{
public:
    GGANullPointerException(const std::string& msg);
};

#endif // _GGA_EXCEPTIONS_HPP_
