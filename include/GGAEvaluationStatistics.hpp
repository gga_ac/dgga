//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                
#ifndef GGAEVALUATIONSTATISTICS_HPP
#define	GGAEVALUATIONSTATISTICS_HPP

#include <boost/serialization/access.hpp>

class GGAEvaluationStatistics 
{
public:
    GGAEvaluationStatistics();
    GGAEvaluationStatistics(const GGAEvaluationStatistics& o);
    virtual ~GGAEvaluationStatistics();

    GGAEvaluationStatistics& operator=(const GGAEvaluationStatistics& o);
    
    void addEvaluation(double performance);    
    void addEvaluation(const GGAEvaluationStatistics&);
    
    double getAvgPerformance() const;
    unsigned getNumberOfEvaluations() const;
private:
    // serialization
    friend class boost::serialization::access;
    template <class Archiver> void serialize(Archiver&, const unsigned int);
    
    // attributes    
    double m_avg_performance;
    unsigned m_nevaluations;
};

// [public in-line] ------------------------------------------------------------

inline double GGAEvaluationStatistics::getAvgPerformance() const
{
    return m_avg_performance;
}

inline unsigned GGAEvaluationStatistics::getNumberOfEvaluations() const
{
    return m_nevaluations;
}

// [serialization] -------------------------------------------------------------

template<typename Archiver>
void GGAEvaluationStatistics::serialize(Archiver& ar, const unsigned int version)
{   
    ar & m_avg_performance;
    ar & m_nevaluations;
}


#endif	/* GGAEVALUATIONSTATISTICS_H */

