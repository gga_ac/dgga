//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                


#ifndef _GGA_TYPEDEFS_HPP_
#define _GGA_TYPEDEFS_HPP_

/* Typedefs of the standard types and collections */

#include <map>
#include <set>
#include <string>
#include <vector>
#include <limits>

#include "GGAValue.hpp"

typedef std::map<std::string, std::string> StringMap;
typedef std::vector<double> DoubleVector;
typedef std::vector<int> IntVector;
typedef std::vector<unsigned> UIntVector;
typedef std::vector<std::string> StringVector;


// Only for the context of the values in a genome
typedef std::map<std::string, GGAValue> GenomeMap;

#endif
