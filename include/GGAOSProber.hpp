//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                

#ifndef _GGA_OS_PROBER_HPP_
#define _GGA_OS_PROBER_HPP_

#if defined(__linux) || defined(__linux__) || defined(linux)
# define OS_LINUX 1
#elif defined(__WIN32__) || defined(__WIN64__)
# define OS_WINDOWS 1
#elif defined(__APPLE__) || defined(__MACH__)
# define OS_OSX 1
#elif defined(__BSD__)
# define OS_BSD 1
#else
# error Unknown OS, check system macros in GGAOSProber.hpp
#endif

#endif // _GGA_OS_PROBER_HPP_
