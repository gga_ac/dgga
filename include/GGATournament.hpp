//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                
#ifndef _GGA_TOURNAMENT_HPP_
#define _GGA_TOURNAMENT_HPP_

#include <ctime>
#include <boost/scoped_ptr.hpp>

#include "GGAGenome.hpp"
#include "GGAInstances.hpp"
#include "GGALearningStrategy.hpp"
#include "GGAPopulation.hpp"
#include "GGASelector.hpp"


// TODO Add option to stop after no improvement in x generations (to be set in options)
class GGATournament
{
public:
    GGATournament(const GGAInstances& instances, GGASelector& selector);
    virtual ~GGATournament();
    
    void populate();
    void run();    

    int getCurrentGeneration() const;

    const GGAPopulation& getPopulation() const;
    GGAPopulation& getPopulation();

    const GGAInstances& getInstances() const;

private:
    // intentionally unimplemented
    GGATournament();
    GGATournament(const GGATournament&);
    GGATournament& operator=(const GGATournament&);

    void initTrajectoryFile() const;
    bool shouldStop() const;

    void nextGeneration();
    const GGAGenomeVector performSelection(const GGAInstanceVector& instances); 
    GGAGenomeVector performCrossover(const GGAGenomeVector& winners) const;
    const GGAGenomeVector& performMutation(GGAGenomeVector& children) const;

    GGAGenomeVector crossoverHelper(const GGAGenomeVector& winners) const;
    int checkNumOld() const;
    int checkNumOld(GGAGenome::Gender gender) const;
    
    void updateInstancesInfo(const GGAInstanceVector& instances, const GGASelectorResult& result);

    // attributes
    GGAInstances instances_;
    GGAPopulation pop_;
    GGAGenome mostFit_;
    GGASelector& selector_;
    boost::scoped_ptr<GGALearningStrategy> learningStrategy_;   
    
    GGAInstanceInfoMap instancesInfo_;
    double instancesInfoAlpha_;   // TODO Make an option
    
    int gen_;
    int evalCount_;    
    int firstGenMaxInstances_; 

    std::vector<double> lastBest_;
    unsigned int numLastBest_;    // TODO Make an option
    double improvementThreshold_; // TODO Make an option
};


//==============================================================================
// GGATournament public in-line methods
   
inline int GGATournament::getCurrentGeneration() const { return gen_; }

inline const GGAPopulation& GGATournament::getPopulation() const { return pop_; }
inline GGAPopulation& GGATournament::getPopulation()             { return pop_; }

inline const GGAInstances& GGATournament::getInstances() const   { return instances_; }

#endif  // _GGA_TOURNAMENT_HPP_
