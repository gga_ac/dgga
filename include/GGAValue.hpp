//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                
#ifndef _GGA_VALUE_HPP_
#define _GGA_VALUE_HPP_

/*
 * GGAValue
 * This class can represent either a double, int or string. It may not have
 * a value for all of its representations and in those cases will return a 
 * default value and print an erorr message.
 * Note that there is no "get" method in this class. This is because the return
 * type could be one of double, int, string. The proper way to retrieve a value is to
 * cast this class into the value you wish to retrieve.
 */

#include <cassert>

#include <iosfwd>
#include <string>

#include <boost/serialization/nvp.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/string.hpp>
#include <boost/scoped_ptr.hpp>


class GGAValue 
{
public:
    enum ValueType { BOOL = 0, LONG = 1, DOUBLE = 2, STRING = 3, UNKNOWN = 4 };

    // public static methods
    static GGAValue getValueFromStr(const std::string& str,
                                    ValueType forceType = UNKNOWN);

    // construct/copy/destruct
    GGAValue();
    GGAValue(const GGAValue&);
    virtual ~GGAValue();

    // value initialization
    GGAValue(bool val);
    GGAValue(long val);
    GGAValue(double val);
    GGAValue(const std::string& val);
    
    // setters
    void setValue(bool val);
    void setValue(long val);
    void setValue(double val);
    void setValue(const std::string& val);

    // getters
    long getLong() const;
    bool getBool() const;
    double getDouble() const;
    const std::string& getString() const;

    // query
    bool hasValue() const;
    bool isLong() const;
    bool isDouble() const;
    bool isString() const;
    bool isBool() const;

    std::string toString() const;    

    GGAValue& operator=(const GGAValue& other);
    bool operator==(const GGAValue& other) const;

private:    
    void resetValues();

    // stream
    friend std::ostream& operator<<(std::ostream& output, const GGAValue& val);

    // serialization
    friend class boost::serialization::access;
    template <class Archiver> void serialize(Archiver&, const unsigned int);
    template <class Archiver> void save(Archiver&, const unsigned int) const;
    template <class Archiver> void load(Archiver&, const unsigned int);

    // Only one value allowed
    ValueType m_type;

    boost::scoped_ptr<bool> m_boolValue;
    boost::scoped_ptr<long> m_longValue;
    boost::scoped_ptr<double> m_dblValue;
    boost::scoped_ptr<std::string> m_strValue;   
};

//==============================================================================
// Public inline methods

/**
 *
 */
inline bool GGAValue::hasValue() const
{ return isLong() || isDouble() || isString() || isBool(); }

/**
 *
 */
inline bool GGAValue::isLong() const 
{ return m_longValue.get() != 0; }

/**
 *
 */
inline bool GGAValue::isDouble() const 
{ return m_dblValue.get() != 0; }

/**
 *
 */
inline bool GGAValue::isString() const 
{ return m_strValue.get() != 0; }

/**
 *
 */
inline bool GGAValue::isBool() const 
{ return m_boolValue.get() != 0; }


//==============================================================================
// serialization

template <class Archiver>
void GGAValue::serialize(Archiver& ar, const unsigned int version)
{
    ar & BOOST_SERIALIZATION_NVP(m_type);    // Save/Load type

    boost::serialization::split_member(ar, *this, version);    
}

template <class Archiver>
void GGAValue::save(Archiver& ar, const unsigned int version) const
{
    switch (m_type) {
        case BOOL:
            assert(isBool());
            ar & BOOST_SERIALIZATION_NVP(*m_boolValue);
            break;
        case LONG:
            assert(isLong());
            ar & BOOST_SERIALIZATION_NVP(*m_longValue);
            break;
        case DOUBLE:
            assert(isDouble());
            ar & BOOST_SERIALIZATION_NVP(*m_dblValue);
            break;
        case STRING:
            assert(isString());
            ar & BOOST_SERIALIZATION_NVP(*m_strValue);
            break;
        case UNKNOWN:
            break;
        default:
            // ERROR: Throw exception or something ;)
            break;
    }
}

template <class Archiver>
void GGAValue::load(Archiver& ar, const unsigned int version)
{
    switch (m_type) {
        case BOOL:
            m_boolValue.reset(new bool());
            ar & BOOST_SERIALIZATION_NVP(*m_boolValue);
            break;
        case LONG:
            m_longValue.reset(new long());
            ar & BOOST_SERIALIZATION_NVP(*m_longValue);
            break;
        case DOUBLE:
            m_dblValue.reset(new double());
            ar & BOOST_SERIALIZATION_NVP(*m_dblValue);
            break;
        case STRING:
            m_strValue.reset(new std::string());
            ar & BOOST_SERIALIZATION_NVP(*m_strValue);
            break;
        case UNKNOWN:
            break;
        default:
            // ERROR: Throw exception or something ;)
            break;
    }
}

#endif // _GGA_VALUE_HPP_
