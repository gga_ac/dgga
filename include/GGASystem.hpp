//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                

#ifndef _GGA_SYSTEM_HPP_
#define _GGA_SYSTEM_HPP_

#include <string>
#include <vector>


/**
 * @return Wall clock time in seconds.
 */
double wallClockTime();


/**
 * @return User time of the current process plus the user time of all its 
 *         children.
 */
double userTime();


/**
 * @return System time of the current process plus the system time of all its
 *         children.
 */
double systemTime();


/**
 * @brief Suspends the execution of the calling thread until the time specified
 *        has elapsed. If a signal resumes the process, it is suspended again
 *        until the process has slept for the specified amount of time.
 *
 * @throw std::runtime_error If any of the system calls fails.
 */
void sleepFor(unsigned seconds, unsigned nanoseconds);


/**
 * @brief Gets the absolute path to the executable file of the running
 *        program.
 * @return A std::string with the absolute path of the running program's
 *         exe file.
 */
std::string getExecutablePath();


/**
 * @brief Retrieves the list of local ip addresses from the system.
 *
 * @return A std::vector of strings with the textual representation of the
 *         ips.
 *
 * @throw std::runtime_error If any of the underlaying system calls fails.
 */
std::vector<std::string> getLocalIPAddrs();


#endif // _GGA_SYSTEM_HPP_
