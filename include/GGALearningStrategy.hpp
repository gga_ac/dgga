//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                

#ifndef _GGA_LEARNING_STRATEGY_HPP_
#define _GGA_LEARNING_STRATEGY_HPP_


#include <vector>
#include "GGAInstances.hpp"


class GGALearningStrategy 
{
public:
    
    // construct/copy/destruct
    GGALearningStrategy(const GGAInstances& instances);
    GGALearningStrategy(const GGALearningStrategy& other);
    virtual ~GGALearningStrategy();
    
    //
    virtual GGAInstanceVector instances(int generation) const;
    virtual GGAInstanceVector selectRandomInstances(size_t n) const;
    GGAInstanceVector selectRandomClusterInstances(
            unsigned index, unsigned n) const;

protected:
    GGAInstances m_instances;
};


//==============================================================================
// GGALearningStrategy public inline methods

/**
 * Default strategy is to just return all instances. Don't use this except for
 * testing.
 */
inline std::vector<GGAInstance> GGALearningStrategy::instances(
        int generation) const
{ return m_instances.getAllInstances(); }


#endif // _GGA_LEARNING_STRATEGY_HPP_
