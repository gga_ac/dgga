//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                
#ifndef _GGA_UTIL_HPP_
#define _GGA_UTIL_HPP_

#include <stdexcept>

#include "ggatypedefs.hpp"
#include "GGAGenome.hpp"
#include "GGAInstances.hpp"
#include "GGALearningStrategies.hpp"
#include "GGAParameterTree.hpp"


// Build a command from the genome
std::string makeCommand(const GGAParameterTree& ptree, const GenomeMap& genome,
                        const GGAInstance& inst); 

std::string makeCommand(const GGAParameterTree& ptree, const GenomeMap& genome,
                        const StringMap& progValues);

// Balance tournament workload
UIntVector balanceTournaments(size_t participants, size_t tourny_size);

// Returns a learning strategy (based on the option specified in GGAOptions)
GGALearningStrategy* createLearningStrategy(const GGAInstances& instances);

// Appends trajectory information to the trajectory file specified in options
void outputTrajectory(const GGAParameterTree& ptree, const GenomeMap& genome,
                      double utime, double wtime);

// Prints all the options in GGAOptions
void printOptions();


/**
 * Workaround to retrieve constant references from std::map in C++03, this 
 * problem is solved in C++11 (map::at()).
 *
 * @throw std::out_of_range if k is not the key of an element in the map.
 */
template <class Map>
const typename Map::mapped_type& mapAt(const Map& map, 
                                       const typename Map::key_type& k)
{
    typename Map::const_iterator it = map.find(k);
    
    if (it == map.end())
        throw std::out_of_range("The given map does not contain any value"
                                " associated to the specified key.");

    return it->second;
}

#endif // _GGA_UTIL_HPP_
