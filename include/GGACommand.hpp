//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                
#ifndef _GGA_COMMAND_HPP_
#define _GGA_COMMAND_HPP_

#include <string>
#include <vector>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>

#include "ggatypedefs.hpp"
#include "GGAInstance.hpp"
#include "GGAParameter.hpp"


class GGACommandArg
{
public:
    enum ArgType {VARIABLE, FIXED};

    // construct/copy/destruct
    GGACommandArg();
    GGACommandArg(const GGACommandArg&);
    GGACommandArg(ArgType type, const std::string& val);
    ~GGACommandArg();

    ArgType type() const;
    void type(ArgType type);

    const std::string& name() const;
    void name(const std::string& name);

    bool isProtected() const;

    const std::string& fixedValue() const;
    
    std::string str() const;

private:
    // serialization
    friend class boost::serialization::access;
    template<class Archiver>void serialize(Archiver&, const unsigned int);

    //
    ArgType m_type;
    std::string m_name;
    std::string m_fixedValue;
    bool m_protected;
};

//==============================================================================
// GGACommandArg typedefs

typedef std::vector<GGACommandArg> CommandArgVector;


/*
 * Represents the command to call the target algorithm from GGA.
 * Variables are set based on their names using a BASH style, 
 * e.g. $this or ${this}
 * Special variables: $seed, $instance
 */
class GGACommand {
public:
    // Static variables
    static StringVector protectedNames();
    
    // construct/destruct
    GGACommand(const std::string& cmd);
    ~GGACommand();
    
    const std::string& rawCommand() const;
    const CommandArgVector& arguments() const;

    void outputTrajectory(const GenomeMap& genome, double utime, double wtime);

    bool isOrPathOk(const GGAParameter::pointer param, 
                    const GenomeMap& genome) const;

    bool validateVarNames(const ParameterMap& params);

    std::string str() const;

private:
    static StringVector* s_protectedNames;

    GGACommand(); // Implemented for serialization
    GGACommand(const GGACommand&); // Intentionally unimplemented

    void parseCommand(const std::string& cmd);

    // serialization
    friend class boost::serialization::access;
    template<class Archiver>void serialize(Archiver&, const unsigned int);

    //
    std::string m_execCommand;
    std::string m_rawCommand;
    CommandArgVector m_commandArgs;
    bool m_parseSuccessful;    
};


//==============================================================================
// GGACommandArg public inline methods

/**
 *
 */
inline GGACommandArg::ArgType GGACommandArg::type() const 
{    return m_type;    }

/**
 *
 */
inline void GGACommandArg::type(GGACommandArg::ArgType type)
{    m_type = type;    }

/**
 *
 */
inline const std::string& GGACommandArg::name() const 
{    return m_name;    }

/**
 *
 */
inline void GGACommandArg::name(const std::string& name)
{    m_name = name;    }

/**
 *
 */
inline bool GGACommandArg::isProtected() const
{    return m_protected;    }

/**
 *
 */
inline const std::string& GGACommandArg::fixedValue() const
{    return m_fixedValue;    }


//==============================================================================
// GGACommandArg private inline/template methods

template<typename Archiver>
void GGACommandArg::serialize(Archiver& ar, const unsigned int version)
{
    ar & m_type;
    ar & m_name;
    ar & m_fixedValue;
    ar & m_protected;
}


//==============================================================================
// GGACommand public inline methods

/**
 *
 */
inline const std::string& GGACommand::rawCommand() const
{    return m_rawCommand;    }

/**
 *
 */
inline const CommandArgVector& GGACommand::arguments() const
{    return m_commandArgs;    }


//==============================================================================
// GGACommand private inline/template methods

template<typename Archiver>
void GGACommand::serialize(Archiver& ar, const unsigned int version)
{
    ar & m_execCommand;
    ar & m_rawCommand;
    ar & m_commandArgs;
    ar & m_parseSuccessful; 
}


#endif // _GGA_COMMAND_HPP_
